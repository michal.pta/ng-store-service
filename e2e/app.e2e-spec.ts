import { StoreServiceDemoPage } from './app.po';

describe('store-service-demo App', () => {
  let page: StoreServiceDemoPage;

  beforeEach(() => {
    page = new StoreServiceDemoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
