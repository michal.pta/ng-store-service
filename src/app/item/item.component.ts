import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { StoreService } from '../store.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input()
  item: any;

  detailsVisible = false;

  constructor(private store: StoreService) { console.log('constructor'); }

  ngOnInit() {
  }

  update() {
    this.store.updateItem(this.item);
  }

}
